package DAOSS;

import enquete.BeanQuestao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DaoQuestao extends BaseDao{
        
    public int Insere(String texto, String resposta, int tipo) throws SQLException, ClassNotFoundException {
        
        BeanQuestao bean = new BeanQuestao(texto,resposta,tipo);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql = "INSERT INTO questao(texto,resposta,tipo) VALUES(?,?,?)";
            stmt2 = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt2.setString(1, bean.getTexto());
            stmt2.setString(2, bean.getResposta());
            stmt2.setInt(3, bean.getTipo());
            
            
            stmt2.execute();
            // FAZER: pegar a chave gerada pela inserção da questão e retornar.
            ResultSet rs = stmt2.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            stmt2.close();
            conn.close();
            return id;
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }
    
    public void Deleta(long id) throws SQLException, ClassNotFoundException {
       
        BeanQuestao bean = new BeanQuestao(id);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql = "DELETE FROM questao WHERE id = ?";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setLong(1, bean.getId());
            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    
    public void Altera(long id, String texto, String resposta) throws SQLException, ClassNotFoundException {
        
        BeanQuestao bean = new BeanQuestao(id,texto,resposta);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql = "UPDATE questao SET texto = ?, resposta = ? WHERE id = " + bean.getId();
            stmt2 = conn.prepareStatement(sql);
            stmt2.setString(1, bean.getTexto());
            stmt2.setString(2, bean.getResposta());
            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    
    public ArrayList<BeanQuestao> getCadastros() {
        ArrayList<BeanQuestao> cadastros = new ArrayList();

        try {
            Connection conn = conectar();
            String sql = "select * from questao"; 
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                cadastros.add(new BeanQuestao(rs.getLong(1), rs.getString(2), rs.getString(3)));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("ERROOOOO: " + ex);
        }

        return cadastros;
    }
    
}

