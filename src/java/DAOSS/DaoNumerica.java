package DAOSS;

import enquete.BeanQuestaoNumerica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoNumerica extends DaoQuestao{
        
    public void Insere(String texto, String resposta, double erro) throws SQLException, ClassNotFoundException {
        
        BeanQuestaoNumerica bean = new BeanQuestaoNumerica(texto,resposta,erro);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql;
            DaoQuestao dao = new DaoQuestao();
            int id_questao = dao.Insere(texto, resposta, 8);
            sql = "INSERT INTO BeanQuestao(questaoNumerica_FK, erro) VALUES(?, ?)";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setLong(1, id_questao);
            stmt2.setDouble(2, bean.getErro());
            

            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
