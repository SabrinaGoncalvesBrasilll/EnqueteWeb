package DAOSS;

import enquete.BeanQuestaoEscolha;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoEscolha extends DaoQuestao{
        
    public void Insere(String texto, String resposta) throws SQLException, ClassNotFoundException {
       
        BeanQuestaoEscolha bean = new BeanQuestaoEscolha(texto,resposta);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql;
            DaoQuestao dao = new DaoQuestao();
            int id_questao = dao.Insere(texto, resposta, 8);
            sql = "INSERT INTO BeanQuestao(questaoEscolha_FK ) VALUES(?)";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setLong(1, id_questao);  
           
            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
