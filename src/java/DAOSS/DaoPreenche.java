package DAOSS;

import enquete.BeanQuestaoPreencheEspacos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoPreenche extends BaseDao{
    
    public void Insere(String texto, String resposta) throws SQLException, ClassNotFoundException {
        
        BeanQuestaoPreencheEspacos bean = new BeanQuestaoPreencheEspacos(texto,resposta);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql;
            DaoQuestao dao = new DaoQuestao();
            int id_questao = dao.Insere(texto, resposta, 8);
            sql = "INSERT INTO BeanQuestao(questaoPreencheEspaço_FK) VALUES(?)";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setLong(1, id_questao);
            

            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
    
