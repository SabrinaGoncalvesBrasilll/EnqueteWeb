package DAOSS;

import enquete.BeanQuestaoTextual;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DaoTextual extends DaoQuestao{
        
    public void InsereTextual(String texto, String resposta, int minChars) throws SQLException, ClassNotFoundException {
    
        BeanQuestaoTextual bean = new BeanQuestaoTextual(texto,resposta,minChars);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql;
            DaoQuestao dao = new DaoQuestao();
            int id_questao = dao.Insere(texto, resposta, 8); // trocar valor do tipo por algo que faça sentido
            sql = "INSERT INTO questaoTextual(questaoTextual_Fk, minimo_char) VALUES(?, ?)";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setLong(1, id_questao);
            stmt2.setInt(2, bean.getMinChars());

            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void AlteraTextual(long id, String texto, String resposta, int caractere) throws SQLException, ClassNotFoundException {
     
        BeanQuestaoTextual bean = new BeanQuestaoTextual(id ,texto,resposta,caractere);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql = "UPDATE questao SET texto = ?, resposta = ?, minimo_char = ? WHERE id = " + bean.getId();
            stmt2 = conn.prepareStatement(sql);
            stmt2.setString(1, bean.getTexto());
            stmt2.setString(2, bean.getResposta());
            stmt2.setLong(3, bean.getMinChars());
            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public ArrayList<BeanQuestaoTextual> getCadastrosTextual() {
        ArrayList<BeanQuestaoTextual> cadastros = new ArrayList();

        try {
            Connection conn = conectar();
            String sql = "select * from questaoTextual"; 
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                cadastros.add(new BeanQuestaoTextual(rs.getString(1), rs.getString(2), rs.getInt(3)));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("ERROOOOO: " + ex);
        }

        return cadastros;
    }
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        DaoTextual dao = new DaoTextual();
        dao.InsereTextual("olá", "-", 120);
        
    }
}
