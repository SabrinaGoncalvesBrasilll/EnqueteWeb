package DAOSS;

import enquete.BeanOpcoes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoOpcoes extends BaseDao{
        
    public void Insere(String texto, String estado) throws SQLException, ClassNotFoundException {
       
        BeanOpcoes bean = new BeanOpcoes(texto,estado);
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = conectar();
            String sql;
            sql = "INSERT INTO BeanQuestao(texto,estado) VALUES(?,?)";
            stmt2 = conn.prepareStatement(sql);
            stmt2.setString(1, bean.getTexto());
            stmt2.setString(2, bean.getEstado());

            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}

