package enquete;
public class BeanOpcoes {
    
    protected String texto;
    protected String estado;
    
    public BeanOpcoes (String texto, String estado){
        this.estado = estado;
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
