package enquete;

public class BeanQuestaoTextual extends BeanQuestao {
    private int minChars;

    public BeanQuestaoTextual(String texto) {
        super(texto, "");
    }

    public BeanQuestaoTextual(long id, String texto, String resposta, int minChar) {
        super(texto, resposta);
        this.resposta = resposta;
        this.minChars = minChar;
        this.id = id;
    }

    public BeanQuestaoTextual(String texto, String resposta, int minChar) {
        super(texto, resposta);
        this.resposta = resposta;
        this.minChars = minChar;
    }
    
    public BeanQuestaoTextual(String texto, int minChars) {
        super(texto, "");
        this.minChars = minChars;
    }

    public int getMinChars() {
        return minChars;
    }

    public void setMinChars(int minChars) {
        this.minChars = minChars;
    }

    @Override
    public boolean verificaResposta(String resposta) {
        return resposta.length() >= minChars && resposta.endsWith(".") && Character.isUpperCase(resposta.charAt(0));
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        return "<h2>"+texto+"</h2><input type=\"text\" name=\""+nomeQuestao+"\"></br>";
    }

}
