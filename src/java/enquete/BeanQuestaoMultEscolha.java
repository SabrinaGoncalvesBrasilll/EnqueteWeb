package enquete;

public class BeanQuestaoMultEscolha extends BeanQuestaoEscolha {


    public BeanQuestaoMultEscolha(String texto) {
        super(texto);
    }

    @Override
    public void adicionaOpcao(String opcao, boolean correto) {
        opcoes.add(opcao);
        if (correto) {
            this.setResposta(this.getResposta() + ((this.getResposta().equals("")) ? "" : ",") + opcoes.size());
        }
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        String result = "<h2>"+texto+"</h2>";
        for (int i = 0; i < this.opcoes.size(); i++) {
            result += "<input type=\"checkbox\" name=\""+nomeQuestao+"\" value=\"" + (i+1)+"\">"+this.opcoes.get(i)+"</br>";
        }
        return result;
    }
}
