package enquete;

import java.util.ArrayList;

public class BeanQuestaoEscolha extends BeanQuestao {

    protected ArrayList<String> opcoes;

    public BeanQuestaoEscolha(String texto, String resposta) {
        super(texto, "");
        this.resposta = resposta;
        this.opcoes = opcoes;
    }
    
    public BeanQuestaoEscolha (String texto){
         super(texto, "");
         this.opcoes = new ArrayList();
    }

    public void adicionaOpcao(String opcao, boolean correto) {
        this.opcoes.add(opcao);
        if (correto) {
            this.setResposta(this.opcoes.size() + "");
        }
    }

    public ArrayList<String> getOpcoes() {
        return opcoes;
    }

    public void setOpcoes(ArrayList<String> opcoes) {
        this.opcoes = opcoes;
    }

    @Override
    public String toString() {
        String result = super.toString() + "\n";
        for (int i = 0; i < this.opcoes.size(); i++) {
            result += String.format("(%d) %s\n", i + 1, this.opcoes.get(i));
        }
        return result;
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        String result = "<h2>"+texto+"</h2>";
        for (int i = 0; i < this.opcoes.size(); i++) {
            result += "<input type=\"radio\" name=\""+nomeQuestao+"\" value=\"" + (i+1)+"\">"+this.opcoes.get(i)+"</br>";
        }
        return result;
    }
            
    
} 
