package enquete;

public class BeanQuestao {
    protected long id;
    protected String texto, resposta;
    protected int tipo;

    public BeanQuestao(long id, String texto, String resposta, int tipo) {
        this.texto = texto;
        this.resposta = resposta;
        this.id = id;
        this.tipo = tipo;
    }
    
    public BeanQuestao(String texto, String resposta, int tipo) {
        this.texto = texto;
        this.resposta = resposta;
        this.tipo = tipo;
    }
    
    public BeanQuestao(long id, String texto, String resposta) {
        this.texto = texto;
        this.resposta = resposta;
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public BeanQuestao(String texto, String resposta) {
        this.texto = texto;
        this.resposta = resposta;
    }

    public BeanQuestao(String texto) {
        this.texto = texto;
    }
    
    public BeanQuestao(long id) {
        this.id = id;
    }
    
    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean verificaResposta(String resposta) {
        return this.resposta.equals(resposta);
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    @Override
    public String toString() {
        return this.texto;
    }
    
    public String toHTML(String nomeQuestao) {
        return "Fazer depois";
    }

    public long getId() {
        return id;
    }
}
