package enquete;

public class BeanQuestaoNumerica extends BeanQuestao {
    private double erro = 0.01;

    public BeanQuestaoNumerica(String texto, String resposta, double erro) {
        super(texto, resposta);
        this.erro = erro;
    }

    public double getErro() {
        return erro;
    }

    public void setErro(double erro) {
        this.erro = erro;
    }
    
    @Override
    public boolean verificaResposta(String r) {
        try {
            double respostaRecebida = Double.parseDouble(r);
            double resposta = Double.parseDouble(this.getResposta());
            return respostaRecebida >= resposta - erro && respostaRecebida <= resposta + erro;
        }
        catch(NumberFormatException nfe) {
            System.out.println(nfe);
        }
        return false;
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        return "<h2>"+texto+"</h2><input type=\"text\" name=\""+nomeQuestao+"\"></br>";
    }
}
