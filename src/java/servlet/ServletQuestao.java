package servlet;

import DAOSS.DaoQuestao;
import enquete.BeanQuestao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/ServletQuestao", "/ServletListar", "/ServletInserir", "/ServletExcluir", "/ServletAlterar"})
public class ServletQuestao extends HttpServlet {

protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, ClassNotFoundException {
    response.setContentType("text/html;charset=UTF-8");
}

@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
     try (PrintWriter out = response.getWriter()) { 
        if (request.getServletPath().endsWith("Listar")) {
            
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Listagem</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<div>");
            out.println("<center><h1>Listagem:</h1>");
            out.println("<table border=\"1\" style=\"width:60%\">");
            out.println("<tr>");
            out.println("<th>ID</th><th>Questao:</th><th>Resposta:</th><th>Tipo:</th><th>Excluir:</th><th>Alterar:</th>");
            out.println("</tr>");
            DaoQuestao list = new DaoQuestao();
            for (BeanQuestao q : list.getCadastros()) {
                out.println("<tr>");
                out.println("<td>" + q.getId() + "</td><td>" + q.getTexto() + "</td><td>" + q.getResposta() + "</td><td>" + q.getTipo() + "</td><td><a href=\"./ServletExcluir?id=" + q.getId() + "\">Excluir</a></td><td><a href=\"./ServletAlterar?id=" + q.getId() + "\">Alterar</a></td>");
              //out.println("<td>" + q.getId() + "</td><td>" + q.getTexto() + "</td><td>" + q.getResposta() + "</td> <td><button onclick=\"myFunction()\">Excluir</button></td> <td><button onclick=\"./ServletAlterar?id=" + q.getId() + "\">Alterar</button></td> ");
                out.println("</tr>");
                out.println("<br>");
            }
            out.println("</table>");
            out.println("<br><a href=\"./inicial.html\">Voltar</a>");
            out.println("</div></center>");
            out.println("</body>");
            out.println("</html>");
        }

        if (request.getServletPath().endsWith("Inserir")){
            response.sendRedirect("inserir.html");
        }
        
        if ((request.getServletPath().endsWith("Excluir"))){
                
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Excluir</title>");
                out.println("<meta charset=\"UTF-8\">");
                out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
                out.println("</head>");
                out.println("<body>");      

                out.println("<center>"); 
                out.println("<div>Deseja realmente excluir essa questao?</div><br>");
                
                out.println("<form name=\"excluir\" method=\"post\" action=\"ServletExcluir\">");
                out.println("<input type= \"hidden\" name=\"id\" value='" + request.getParameter("id") + "'>");
                out.println("<input type=\"submit\" value=\"Com toda a certeza!\"><br>");
                out.println("</form>");
                
                out.println("<form name=\"excluir\" method=\"get\" action=\"ServletListar\">");
                out.println("<input type=\"submit\" value=\"Naoooooo!!!!\">");                
                out.println("</form>");

                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
           }
        
        if ((request.getServletPath().endsWith("Alterar"))){
                
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Alterar</title>");
                out.println("<meta charset=\"UTF-8\">");
                out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
                out.println("</head>");
                out.println("<body>");      

                String perg = "", resp = "";
                DaoQuestao list = new DaoQuestao();
                for (BeanQuestao q : list.getCadastros()) {
                    if (q.getId() == Long.parseLong(request.getParameter("id"))){
                        perg = q.getTexto();
                        resp = q.getResposta();
                        break;
                    }
                }
                
                out.println("<center><form name=\"inserir\" method=\"post\" action=\"ServletAlterar\">");
                out.println("Pergunta:");
                out.println("<input type=\"text\" name=\"texto\" value=\'" + perg + "\'><br><br>");
                out.println("Resposta:");
                out.println("<input type= \"hidden\" name=\"id\" value=\'" + request.getParameter("id") + "'>");
                out.println("<input type=\"text\" name=\"resposta\"  value=\'" + resp + "\'><br><br>");
                out.println("<input type=\"submit\" value=\"Salvar\">");
                out.println("</form>");

                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
           }
            
        }    

        if (request.getServletPath().endsWith("Alterar")){

            long l = Long.parseLong(request.getParameter("id"));
            response.sendRedirect("inserir.html");
            
        }
}


@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        if (request.getServletPath().endsWith("Inserir")){

            DaoQuestao dao = new DaoQuestao();
            String texto = request.getParameter("texto");
            String resposta = request.getParameter("resposta");

            try {
                dao.Insere(texto, resposta, 1);
                response.sendRedirect("inicial.html");
            } catch (SQLException ex) {
                System.out.println(ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletQuestao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (request.getServletPath().endsWith("Alterar")){
            
            DaoQuestao dao = new DaoQuestao();
            long l = Long.parseLong(request.getParameter("id"));
            String texto = request.getParameter("texto");
            String resposta = request.getParameter("resposta");

            try {
                dao.Altera(l,texto,resposta);
                response.sendRedirect("./ServletListar");
            } catch (SQLException ex) {
                System.out.println(ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletQuestao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (request.getServletPath().endsWith("Excluir")){
            
            DaoQuestao dao = new DaoQuestao();   
            long l = Long.parseLong(request.getParameter("id"));

            try {
                dao.Deleta(l);
                response.sendRedirect("./ServletListar");
            } catch (SQLException ex) {
                System.out.println(ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletQuestao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
