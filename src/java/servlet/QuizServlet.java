package servlet;
import enquete.BeanQuestao;
import enquete.BeanQuestaoEscolha;
import enquete.BeanQuestaoMultEscolha;
import enquete.BeanQuestaoNumerica;
import enquete.BeanQuestaoPreencheEspacos;
import enquete.BeanQuestaoTextual;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/QuizServlet"})
public class QuizServlet extends HttpServlet {

    ArrayList<BeanQuestao> questoes = null;

    @Override
    public void init()
            throws ServletException {

        questoes = new ArrayList();

        BeanQuestaoEscolha q = new BeanQuestaoEscolha("1) Qual o nome do criador de Java?");
        q.adicionaOpcao("James Gosling", true);
        q.adicionaOpcao("Guido Van Rossum", false);
        q.adicionaOpcao("Bjarne Stroustrup", false);
        q.adicionaOpcao("Larry Wall", false);
        q.adicionaOpcao("Brendan Eich", false);

        questoes.add(q);

        q = new BeanQuestaoMultEscolha("2) Quais das linguagens abaixo rodam sobre VMs?");
        q.adicionaOpcao("Java", true);
        q.adicionaOpcao("C++", false);
        q.adicionaOpcao("Python", true);
        q.adicionaOpcao("Dart", true);
        q.adicionaOpcao("C", false);

        questoes.add(q);

        BeanQuestaoPreencheEspacos qpe = new BeanQuestaoPreencheEspacos("3) O criador de Java foi _James Gosling_, na década de 90.");

        questoes.add(qpe);

        BeanQuestaoNumerica qn = new BeanQuestaoNumerica("4) 1 + 0.001 é:", "1.00", 0.01);

        questoes.add(qn);

        BeanQuestaoTextual qt = new BeanQuestaoTextual("5) Escreva algo.", 10);

        questoes.add(qt);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Quiz </h1>");
            out.println("<form action=\"./QuizServlet\" method=\"post\">");

            int i = 0;
            for (BeanQuestao q : questoes) {
                out.println(q.toHTML("q" + (++i)));
            }
            out.println("<br><input type=\"submit\" value=\"enviar\">");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            out.println("<html>");
            out.println("<head>");
            out.println("<link href = \"OurCss.css\" rel = \"stylesheet\" type = \"text/css\"/>");
            out.println("<title>Servlet QuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            
            int i = 0;

            out.println("<center><br><h1>Relatório:</h1><br><br>");
            out.println("<table border=\"1\" style=\"width:10%\">");
            out.println("<tr>");
            out.println("<th>Questao:</th><th>Resposta:</th>");
            out.println("</tr>");
             
            for (BeanQuestao q : questoes) {
                
                String valores[] = request.getParameterValues("q" + (++i));

                if (valores == null) {
                out.println("<tr>");
                out.println("<td>" + i + "</td><td class = \"errado\">Vc não respondeu</td>");
                out.println("</tr>");
                out.println("<br>");
                } else {
                    String resposta = "";

                    for (int j = 0; j < valores.length - 1; j++) {
                        resposta += valores[j] + ",";
                    }

                    resposta += valores[valores.length - 1];
                    if (!resposta.equals("")) {
                        if (q.verificaResposta(resposta)) {
                            out.println("<tr>");
                            out.println("<td>" + i + "</td><td class = \"certa\">Correta</td>");
                            out.println("</tr>");
                            out.println("<br>");
                        } else {
                            out.println("<tr>");
                            out.println("<td>" + i + "</td><td class = \"errado\">Errada</td>");
                            out.println("</tr>");
                            out.println("<br>");
                        }
                    } else {
                        out.println("<tr>");
                        out.println("<td>" + i + "</td><td class = \"errado\">Vc não respondeu</td>");
                        out.println("</tr>");
                        out.println("<br>");
                    }
                }
            }
            
        out.println("</table>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        }   
    }
}
