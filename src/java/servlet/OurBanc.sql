drop database if exists MPS ;
create database MPS;
use MPS;

create table tipo(
    id integer auto_increment,
    descricao varchar(100) not null,
    primary key (id)
);

create table questao(
    id integer auto_increment,
    texto varchar(100) not null,
    resposta varchar(100) not null,
    tipo integer,
    primary key (id)
--     foreign key (tipo) references tipo(id)
);

create table questaoNumerica(
    id integer auto_increment,
    questaoNumerica_FK integer not null,
    erro integer not null,
    primary key (id),
    foreign key (questaoNumerica_FK) references questao (id)
);

create table questaoEscolha(
    id integer auto_increment,
    questaoEscolha_FK integer not null,
    primary key (id),
    foreign key (questaoEscolha_FK) references questao (id)
);

create table questaoTextual(
    id integer auto_increment,
    minimo_char integer not null,
    questaoTextual_Fk integer not null,
    primary key (id),
    foreign key (questaoTextual_FK) references questao (id)
);

create table questaoMultiEscolha(
    id integer auto_increment,
    questaoEscolha_FK integer not null,
    primary key (id),
    foreign key (questaoEscolha_FK) references questaoEscolha (id)
);

create table questaoPreencheEspaço(
    id integer auto_increment,
    questaoPreencheEspaço_FK integer not null,
    primary key (id),
    foreign key (questaoPreencheEspaço_FK) references questao (id)
);

create table opcoes(
    qe_id int not null,
    descricao varchar(50) not null,
    estado varchar(10),
    primary key(qe_id),
    foreign key (qe_id) references questaoEscolha (id)
);

insert into tipo (descricao) values ('Númerica');
insert into tipo (descricao) values ('Preenche Espaço');
insert into tipo (descricao) values ('Escolha');
insert into tipo (descricao) values ('Textual');
insert into tipo (descricao) values ('Munti Escolhas');
insert into questao (texto, resposta, tipo) values ('Passa teu whats!','Não, tô bem!', 1);
insert into questao (texto, resposta, tipo) values ('Oi!','Seila!', 2);

select * from questaoTextual;
/*insert into questao values (...);
insert into Questãonumerica values (last_insert_id(), ...);*/
