package servlet;

import DAOSS.DaoQuestao;
import DAOSS.DaoTextual;
import enquete.BeanQuestao;
import enquete.BeanQuestaoTextual;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/ServletTextual", "/ServletTextualInserir", "/ServletTextualAlterar"})
public class ServletTextual extends HttpServlet {

protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, ClassNotFoundException {
    response.setContentType("text/html;charset=UTF-8");
}

@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
     try (PrintWriter out = response.getWriter()) { 
        
        if (request.getServletPath().endsWith("TextualInserir")){
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Inserir</title>");
                out.println("<meta charset=\"UTF-8\">");
                out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
                out.println("</head>");
                out.println("<body>");
                out.println("<div>");
                out.println("<center><form name=\"inserir\" method=\"post\" action=\"ServletTextualInserir\">");
                out.println("Pergunta:");
                out.println("<input type=\"text\" name=\"pergunta\"><br><br>");
                out.println("Resposta:");
                out.println("<input type=\"text\" name=\"resposta\"><br><br>");
                out.println("Minimo de caracteres:");
                out.println("<input type=\"number\" name=\"caracteres\" min=\"1\"><br><br>");
                out.println("<input type=\"submit\" value=\"Salvar\">");
                out.println("</form></center>");
                out.println("</div>");
                out.println("</body>");
                out.println("</html>");
        }
        
        if ((request.getServletPath().endsWith("TextualAlterar"))){
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Alterar</title>");
                out.println("<meta charset=\"UTF-8\">");
                out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
                out.println("</head>");
                out.println("<body>");      

                String perg = "", resp = "";
                int carac = 1;
                DaoTextual list = new DaoTextual();
                for (BeanQuestaoTextual q : list.getCadastrosTextual()) {
                    if (q.getId() == Long.parseLong(request.getParameter("id"))){
                        perg = q.getTexto();
                        resp = q.getResposta();
                        carac = q.getMinChars();
                        break;
                    }
                }
                out.println("<center><form name=\"inserir\" method=\"post\" action=\"ServletTextualAlterar\">");
                out.println("Pergunta:");
                out.println("<input type=\"text\" name=\"texto\" value=\'" + perg + "\'><br><br>");
                out.println("Resposta:");
                out.println("<input type= \"hidden\" name=\"id\" value=\'" + request.getParameter("id") + "'>");
                out.println("<input type=\"text\" name=\"resposta\"  value=\'" + resp + "\'><br><br>");
                out.println("Mínimo de caracteres:");
                out.println("<input type=\"number\" name=\"caracteres\" min=\"1\" value=\'" + carac + "><br><br>");
                out.println("<input type=\"submit\" value=\"Salvar\">");
                out.println("</form>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
           }
        }    

        /*if (request.getServletPath().endsWith("Alterar")){

            long l = Long.parseLong(request.getParameter("id"));
            response.sendRedirect("inserir.html");
            
        }*/
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        if (request.getServletPath().endsWith("TextualInserir")){

            DaoTextual dao = new DaoTextual();
            String texto = request.getParameter("texto");
            String resposta = request.getParameter("resposta");
            int caracteres = Integer.parseInt(request.getParameter("caracteres"));

            try {
                dao.InsereTextual(texto, resposta, caracteres);
                response.sendRedirect("inicial.html");
            } catch (SQLException ex) {
                System.out.println(ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletQuestao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (request.getServletPath().endsWith("TextualAlterar")){
            
            DaoTextual dao = new DaoTextual();
            long id = Long.parseLong(request.getParameter("id"));
            String texto = request.getParameter("texto");
            String resposta = request.getParameter("resposta");
            int caractere = Integer.parseInt(request.getParameter("caracteres"));

            try {
                dao.AlteraTextual(id, texto, resposta, caractere);
                response.sendRedirect("./ServletListar");
            } catch (SQLException ex) {
                System.out.println(ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletQuestao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
