package servlet;

import enquete.Questao;
import enquete.QuestaoEscolha;
import enquete.QuestaoMultEscolha;
import enquete.QuestaoNumerica;
import enquete.QuestaoPreencheEspacos;
import enquete.QuestaoTextual;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/QuizServlet"})
public class QuizServlet extends HttpServlet {

    ArrayList<Questao> questoes = null;

    @Override
    public void init()
            throws ServletException {

        questoes = new ArrayList();

        QuestaoEscolha q = new QuestaoEscolha("1) Qual o nome do criador de Java?");
        q.adicionaOpcao("James Gosling", true);
        q.adicionaOpcao("Guido Van Rossum", false);
        q.adicionaOpcao("Bjarne Stroustrup", false);
        q.adicionaOpcao("Larry Wall", false);
        q.adicionaOpcao("Brendan Eich", false);

        questoes.add(q);

        q = new QuestaoMultEscolha("2) Quais das linguagens abaixo rodam sobre VMs?");
        q.adicionaOpcao("Java", true);
        q.adicionaOpcao("C++", false);
        q.adicionaOpcao("Python", true);
        q.adicionaOpcao("Dart", true);
        q.adicionaOpcao("C", false);

        questoes.add(q);

        QuestaoPreencheEspacos qpe = new QuestaoPreencheEspacos("3) O criador de Java foi _James Gosling_, na década de 90.");

        questoes.add(qpe);

        QuestaoNumerica qn = new QuestaoNumerica("4) 1 + 0.001 é:", "1.00", 0.01);

        questoes.add(qn);

        QuestaoTextual qt = new QuestaoTextual("5) Escreva algo.", 10);

        questoes.add(qt);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Quiz </h1>");
            out.println("<form action=\"./QuizServlet\" method=\"post\">");

            int i = 0;
            for (Questao q : questoes) {
                out.println(q.toHTML("q" + (++i)));
            }
            out.println("<br><input type=\"submit\" value=\"enviar\">");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link href = \"OurCss.css\" rel = \"stylesheet\" type = \"text/css\"/>");
            out.println("<title>Servlet QuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            
            int i = 0;

            out.println("<center><br><h1>Relatório:</h1><br><br>");
            out.println("<table border=\"1\" style=\"width:100%\">");
            out.println("<tr>");
            out.println("<th>Questao:</th><th>Resposta:</th>");
            out.println("</tr>");
             
            for (Questao q : questoes) {
                
                String valores[] = request.getParameterValues("q" + (++i));

                if (valores == null) {
                out.println("<tr class = \"errado\">");
                out.println("<td>" + i + "</td><td>Vc não respondeu</td>");
                out.println("</tr>");
                out.println("<br>");
                } else {
                    String resposta = "";

                    for (int j = 0; j < valores.length - 1; j++) {
                        resposta += valores[j] + ",";
                    }

                    resposta += valores[valores.length - 1];
                    if (!resposta.equals("")) {
                        if (q.verificaResposta(resposta)) {
                            out.println("<tr class = \"certa\">");
                            out.println("<td>" + i + "</td><td>Correta</td>");
                            out.println("</tr>");
                            out.println("<br>");
                        } else {
                            out.println("<tr class = \"errado\">");
                            out.println("<td>" + i + "</td><td>Errada</td>");
                            out.println("</tr>");
                            out.println("<br>");
                        }
                    } else {
                        out.println("<tr class = \"errado\">");
                        out.println("<td>" + i + "</td><td>Vc não respondeu</td>");
                        out.println("</tr>");
                        out.println("<br>");
                    }
                }
            }
            
        out.println("</table>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        }   
    }
}
