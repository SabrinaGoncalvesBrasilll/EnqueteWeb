create schema MPS ;
use schema MPS;

create table questao(
id integer auto_increment,
texto varchar(100) not null,
resposta varchar(100) not null,
primary key (id));

create table questaoNumerica(
id integer auto_increment,
questao_id integer not null,
erro integer not null,
primary key (id),
add constraint questaoNumerica_FK,
foreign key (questao_id) 
references questao (id));

create table questaoEscolha(
id integer auto_increment,
questao_id integer not null,
primary key (id),
add constraint questaoEscolha_FK,
foreign key (questao_id) 
references questao (id));

create table questaoTextual(
id integer auto_increment,
questao_id integer not null,
minimo_char int not null,
primary key (id),
add constraint questaoNumerica_FK,
foreign key (questao_id) 
references questao (id));

create table questaoMultiEscolha(
id integer auto_increment,
questaoEscolha_id int not nnull,
primary key (id)
add constraint questaoEscolha_FK,
foreign key (questaoEscolha_id)
references questaoEsolha (id));

create table questaoPreencheEspaço(
id integer auto_increment,
questao_id integer not null,
primary key (id),
add constraint questaoPreencheEspaço_FK,
foreign key (questao_id) 
references questao (id));

insert into questao values (...);
insert into Questãonumerica values (last_insert_id(), ...);