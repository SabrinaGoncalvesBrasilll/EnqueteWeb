package enquete;

public class Questao {
    
    protected String texto, resposta;

    public Questao(String texto, String resposta) {
        this.texto = texto;
        this.resposta = resposta;
    }

    public boolean verificaResposta(String resposta) {
        return this.resposta.equals(resposta);
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    @Override
    public String toString() {
        return this.texto;
    }
    
    public String toHTML(String nomeQuestao) {
        return "Fazer depois";
    }
}
