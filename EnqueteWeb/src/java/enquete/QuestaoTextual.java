package enquete;

public class QuestaoTextual extends Questao {
    private int minChars = 100;

    public QuestaoTextual(String texto) {
        super(texto, "");
    }

    public QuestaoTextual(String texto, int minChars) {
        super(texto, "");
        this.minChars = minChars;
    }

    @Override
    public boolean verificaResposta(String resposta) {
        return resposta.length() >= minChars && resposta.endsWith(".") && Character.isUpperCase(resposta.charAt(0));
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        return "<h2>"+texto+"</h2><input type=\"text\" name=\""+nomeQuestao+"\"></br>";
    }

}
