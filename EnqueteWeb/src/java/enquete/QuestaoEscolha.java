package enquete;

import java.util.ArrayList;


public class QuestaoEscolha extends Questao {

    protected ArrayList<String> opcoes;

    public QuestaoEscolha(String texto) {
        super(texto, "");
        this.opcoes = new ArrayList<String>();
    }

    public void adicionaOpcao(String opcao, boolean correto) {
        this.opcoes.add(opcao);
        if (correto) {
            this.setResposta(this.opcoes.size() + "");
        }
    }

    @Override
    public String toString() {
        String result = super.toString() + "\n";
        for (int i = 0; i < this.opcoes.size(); i++) {
            result += String.format("(%d) %s\n", i + 1, this.opcoes.get(i));
        }
        return result;
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        String result = "<h2>"+texto+"</h2>";
        for (int i = 0; i < this.opcoes.size(); i++) {
            result += "<input type=\"radio\" name=\""+nomeQuestao+"\" value=\"" + (i+1)+"\">"+this.opcoes.get(i)+"</br>";
        }
        return result;
    }
            
    
} 
