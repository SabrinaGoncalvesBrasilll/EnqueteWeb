package enquete;

public class QuestaoPreencheEspacos extends Questao {

    public QuestaoPreencheEspacos(String texto) {
        // "_.+_" é uma expressão regular que casa com qualquer string que tenha algo entre underscores (_)
        super(texto.replaceAll("_.+_", "____"), texto.split("_")[1]);
    }
    
    @Override
    public String toHTML(String nomeQuestao) {
        return "<h2>"+texto+"</h2><input type=\"text\" name=\""+nomeQuestao+"\"></br>";
    }
}
