package enquete;

public class Quiz {
    public static void main(String[] args) {
        QuestaoEscolha q = new QuestaoEscolha("Qual o nome do criador de Java?");
        q.adicionaOpcao("James Gosling", true);
        q.adicionaOpcao("Guido Van Rossum", false);
        q.adicionaOpcao("Bjarne Stroustrup", false);
        q.adicionaOpcao("Larry Wall", false);
        q.adicionaOpcao("Brendan Eich", false);
        System.out.println(q);
        System.out.println(q.verificaResposta("1"));

        QuestaoMultEscolha mq = new QuestaoMultEscolha("Quais das linguagens abaixo rodam sobre VMs?");
        mq.adicionaOpcao("Java", true);
        mq.adicionaOpcao("C++", false);
        mq.adicionaOpcao("Python", true);
        mq.adicionaOpcao("Dart", true);
        mq.adicionaOpcao("C", false);
        System.out.println(mq);
        System.out.println(mq.verificaResposta("1,3,4"));

        QuestaoPreencheEspacos fq = new QuestaoPreencheEspacos("O criador de Java foi _James Gosling_, na década de 90.");
        System.out.println(fq);
        System.out.println(fq.verificaResposta("James Gosling"));

        QuestaoNumerica nq = new QuestaoNumerica("1 + 0.001 é: ", "1.00", 0.01);
        System.out.println(nq);
        System.out.println(nq.verificaResposta("1.001"));

        QuestaoTextual qt = new QuestaoTextual("Escreva algo.", 10);
        System.out.println(qt);
        System.out.println(qt.verificaResposta("Ola ola ola ola."));
    }
}
